# Docker Workshop
Demo 04: Kubernetes Overview

---

## Preparations

 - Clone the lab repository:
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/credorax-docker-workshop/demo-04.git ~/demo-04
$ cd ~/demo-04
```

## Configure kubectl CLI

 - Install 'kubectl' using snap
```
sudo snap install kubectl --classic
```

 - Ensure kubectl is installed and check its version
```
kubectl version
```

 - Download youre newly created Kubernetes cluster credentials and configure kubectl
```
gcloud container clusters get-credentials $(hostname) --zone europe-west1-c --project devops-course-architecture
```

 - Inspect kubectl configuration file
```
cat ~/.kube/config
```

 - List cluster nodes
```
kubectl get nodes
```

## Deploy the application

 - Inspect the configuration files under "config-k8s"
 
 - Deploy everything under "config-k8s":

```
kubectl create -f ./config-k8s/
```

 - Show deployments and services:

```
kubectl get deployments
kubectl get services
```

 - Browse to the application (vote, result)

 - Scale the vote service to 3 (modify configuration file)

``` 
kubectl edit deployment vote
kubectl get deployments
```

 - Show running pods:

```
kubectl get pods
```

 - Create namespace (feature environment) - Show configuration
```
kubectl create -f ./config-k8s-namespace/feature-namespace.yaml
```

 - Create deployments and services in new environment (change services ports + namespace + docker images)
```
kubectl create -f ./config-k8s-updated
```

 - Show deployments and services:

```
kubectl get deployments -n feature
kubectl get services -n feature
```

 - Show running in all the cluster:

```
kubectl get pods --all-namespaces
```

 - Browse to the application

---

## CLEANUP

```
kubectl delete namespace feature
kubectl delete services --all
kubectl delete deployments --all
```





